


1. 인증 방식 결정 

- 네이버 방식 :  oauch2 방식을 채용 또는 jwt 로 구성 
- socket 접속시에 access Token 을 발행함
- 해당 access Token으로 인증후에는 아래와 같이 세션키를 다시줌 




2. 인증 로직 정리 

- 사용자 로그인 인증 후 
- JWT ACCESS TOKEN 발행
- 인증 후 언제까지 엑세스 토큰을 유효 처리 할것 인지 결정(1day)
- 다른 IP 또는 기계로 접속시 알람 표시




```
sessionKey: "NID_AUT=TdQRPI4REiaRC6QUzDHdvgG3CVhMu8BB/uiTIfd0AXaBTipXVXesuA8mNLKDVBD+; NID_SES=AAABnJpHpIow6sbtyS3hdE6siO4bax9Tc85qvpiVkiE3kj3uoqW75wey6Z1L/NmFxledKOjTcMGE0vxGC7+Ub4g5S1AiYBmsSgERdrQrDRHEOQ7kp1rNgslF4Fp2ogmWrlKpg8YrHBdwThLK0RKrDRdFNEtghhm8ICNzsXW9Sx1H530enV06pcpC9zXy1hzFtcBlmKoZUBH6c07Fc2pnqeFmFqbMqrTHXQCH6R0MQQM1QMPOhxy8yhAmCp5ciWBbOsk3WJC6DGaJhFN5Pklm0/AjzbRsyWYcHW1lMWqKbrcB12tv8DYeYdlf0+Ka74y9+SGFUlUftZ913NjXDXE0SQ1ngtFCis/3Qg4svZI+YNh3s/1ZkJJlSimiw5HkK6zj8MqmRkUnTWx6lDagr7ihqsx8J5yc+pH9j9k+m8kzZIRF9yMMI9mRyYUcsfhdzJr4PXzcUClcSKN+b6/7YWQyA0WmEnF+1ghroMv1MzhfTCN7Oh68Am9qlTJxxKGWNaAQMj6oMGihXB9/SGh3Ueev4djJm+xD+qJt66OD05AVnNjyLTg7;"
```