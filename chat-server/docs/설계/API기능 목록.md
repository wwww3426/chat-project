# 1. API 목록

- Channel API
- Chat API
- Room API

## 1.1 Channel API 목록

- Channel 신규 생성(Create)
- Channel 수정(Update)
- Channel 삭제(Delete)
- Channel 목록 조회(LIST)
- Channel 상세 정보 조회(DETAIL)
- Channel 추가(Add) - User가 사용 하는 채널을 추가 한다.


## 1.2 Room API 목록

- Room 신규 생성(CREATE)
- Room 수정(UPDATE)
- Room 삭제(DELETE)
- Room 목록 조회(LIST)
- Room 상세 정보 조회(DETAIL)

- Room 입장(ENTER)
- Room 초대(INVITE)
- Room 강퇴(FORCED EXIT) 
- Room 나가기(EXIT)



## 1.3 Chat API 목록

- Chat 대화 보내기
- Chat 대화 받기
- Chat 대화 읽은수 표시
- Chat 입력 상태 표시
- Chat User 접속 상태 표시

 