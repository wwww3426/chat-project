

import bodyParser from "body-parser";
import compression from "compression"; // compresses requests
import connectRedis from "connect-redis";
import express from "express";
import session from "express-session";
import { createServer } from "http";
import path from "path";
import socketIO from "socket.io";
import ioRedis from "socket.io-redis";
import * as swaggerUI from "swagger-ui-express";
// swagger Setting
import * as swaggerDocument from "../swagger.json";
import { ioHandler } from "./io-handler";
import { SwaggerAPIRouter, UserRouter } from "./routes";

// DB 연결TEST


const app = express();
app.use(compression());

// XXX: server에서 포트를 꼭 바꾸어야 한다. app 하면 웹소켓 안돈다 ...
const server = createServer(app);


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// 레디스로 세션 클러스터링에 대응
const RedisStore = connectRedis(session);
app.use(session({
  store: new RedisStore({
    host: "localhost",
    port: 6379,
    logErrors: true
  }),
  secret: "s3Cur3",
  resave: false,
  // saveUninitialized: true,
  cookie: {
    // secure: true,
    // httpOnly: false,
    // domain: "localhost",
    // path: "",
    expires: new Date(Date.now() + 60 * 60 * 1000)
  }
}));


// Socket.io
const io = socketIO(server, {
  pingTimeout: 8000,
  pingInterval: 4000
});

// 클러스터링시에 레디스 통해서 전파
io.adapter(ioRedis({ host: "localhost", port: 6379 }));

app.use(
  express.static(path.join(__dirname, "public"))
);

// Swagger EndPoint
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocument));
app.use("/api/v1", SwaggerAPIRouter);
app.use("/user", UserRouter);


ioHandler(io);

// Express configuration
app.set("port", process.env.PORT || 3000);


export default server;
