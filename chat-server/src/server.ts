import "reflect-metadata";
import DbConnect from "./db/DbConnect";
import app from "./app";


/**
 * Error Handler. Provides full stack - remove for production
 */
// app.use(errorHandler());
async function bootstrap() {

  const db = await DbConnect.initialize();

  const server = await app.listen(3000, () => {

    // console.log(
    //   "  App is running at http://localhost:%d in %s mode",
    //   // app.get("port"),
    //   // app.get("env")
    // );
    console.log("  Press CTRL-C to stop\n");
  });
  return server;
}
bootstrap();
/**
 * Start Express server.
 */

