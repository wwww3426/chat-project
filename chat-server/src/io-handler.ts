import { Server } from "socket.io";
import { parse } from "url";
import { chatHandler } from "./handlers/chat-handler";
export enum EVENT_TYPES {
    CONNECTION = "connection",
    ENTER_CHANEL = "ENTER_CHANEL",
    ENTER_ROOM = "ENTER_ROOM",
    ALL_LEAVE_ROOM = "ALL_LEAVE_ROOM",
    LEAVE_ROOM = "LEAVE_ROOM"
}


export const ioHandler = (io: Server) => {

    // TODO: 공통 io 핸들링 로직 구현
    io.sockets.on(EVENT_TYPES.CONNECTION, (socket) => {
        // const { query } = parse(socket.handshake.url, true);
        // const test = socket.handshake.url;
        // const { channel } = query;
        // console.log("io.sockets.on2:");
        console.log("namespace:", Object.keys(io.nsps));
    });

    // chat 핸들러
    const chatNsp = chatHandler(io);


};