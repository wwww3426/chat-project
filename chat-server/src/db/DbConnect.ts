
import { MongoClient, MongoClientOptions, Db } from "mongodb";

// Connection URL
const DB_URL = "mongodb://localhost:27017/kira";
const DB_NAME = "kira";
const options: MongoClientOptions = {
    reconnectTries: 123456,
    reconnectInterval: 123456,
    poolSize: 4,
    appname: "kira"
};
class DbConnect {
    public mongoClient: MongoClient;
    public dataBase: Db;
    public constructor() { }

    public initialize() {
        return this._initialize(DB_URL, options);
    }
    public async _initialize(url: string, config?: MongoClientOptions) {
        if (!this.mongoClient) {
            this.mongoClient = await MongoClient.connect(url, config);
            this.initializedDataBase();
        }
        return this.mongoClient;
    }
    private initializedDataBase(dbName = DB_NAME) {
        this.dataBase = this.mongoClient.db(DB_NAME);
    }

    getMongoClient(): MongoClient {
        return this.mongoClient;
    }
    getDataBase() {
        return this.dataBase;
    }

    getCollection<T>(name: string) {
        return this.getDataBase().collection<T>(name);
    }

    getCollections(name: string) {
        return this.getDataBase().collections();
    }

}


export default new DbConnect();