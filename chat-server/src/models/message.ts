export interface Message {
    channelNo: string;
    messageNo: number;
    content: string;
    updateTime: Date;
    createTime: Date;
    memberCount: number;
    readCount: number;
    // emotion: null;
    extras: string;
    // messageStatusType: "NORMAL";
    // messageTypeCode: 1;
    // tid: 348531800;

}