
export interface User {
    id: string;
    name: string;
    nickName: string;
}