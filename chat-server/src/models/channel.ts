

interface Channel {
    channelId: string;
    createdAt: Date;
    updatedAt: Date;
    userCount: number;
    name: string;
    userFirstMessageNo: number;
    userLatestMessageNo: number;
    description: string;
    visible: boolean;
    // latestMessage: {id: 21857, body: "주말은 역시 놀아야 재미있는데 ㅠ", writerId: "wwww3426", writerName: "비행기", type: 1, …};
    // messagePeriod: 1;
    // newMessageCount: 0;
    // open: true;
    // originalOwner: false;
    // owner: { memberId: "justhe1130", maskingId: "just****", nickname: "우노ESFP 9w8", memberProfileImageUrl: "https://blogpfthumb-phinf.pstatic.net/20150709_222…130_1436368566512Bh8gb_JPEG/939563208.jpg?type=s1", manager: false, … };
    // thumbnailList: ["https://ssl.pstatic.net/cafechat.phinf/MjAxODA5MTJ…rzWeMg.JPEG.justhe1130/d0062547_4cddc28c53f0a.jpg"];
    // type: 4;
    // unreadCountVisible: true;
    // userPushType: "PUSH";
    // userStatus: "JOIN";
}