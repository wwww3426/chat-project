import { Collection } from "mongodb";
import { User } from "../models/user";
import DbConnect from "../db/DbConnect";

function DocumentName(documentName: string) {
    // console.log(param); // {someValue: "hello!"}
    return function <T extends { new(...args: any[]): {} }>(constructor: T) {
        console.log(constructor);
        return class extends constructor {
            documentName = documentName;
        };
    };
}

class CRUDRepository<T> {
    documentName: string;
    constructor() {}
    repository: Collection;

    insert<T>(t: T) {
        DbConnect.getCollection<T>(this.documentName).insertOne(t);
    }


}


@DocumentName("User")
class UserRepository extends CRUDRepository<User> {
    constructor() {
        super();
        // console.log(this);
    }

}

export default new UserRepository();