import { Request, Response } from "express";
import dbConnect from "../db/DbConnect";
import { User } from "../models/user";
import userRepository from "../repository/user-repository";


class UserController {
    constructor() { }
    public async createUser(req: Request, res: Response) {

        await userRepository.insert(<User>{
            id: "TEST",
            name: "TEST",
            nickName: "NickName",
        });

        res.send("성공");
    }
    public async deleteUser(req: Request, res: Response) {

    }
    public async updateUser(req: Request, res: Response) {

    }
    public async getUser(req: Request, res: Response) {

    }
    public async getUsers(req: Request, res: Response) {

    }

}

export default new UserController();