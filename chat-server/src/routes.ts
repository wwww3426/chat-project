import { Router } from "express";
import UserController from "./controller/user.ctr";

const SwaggerAPIRouter = Router();
const UserRouter = Router();
UserRouter.get("/", UserController.createUser);
// UserRouter.get("/", UserController.getUser);
// UserRouter.get("/", UserController.getUsers);
// UserRouter.put("/", UserController.updateUser);
// UserRouter.delete("/", UserController.deleteUser);


export { UserRouter };

export { SwaggerAPIRouter };