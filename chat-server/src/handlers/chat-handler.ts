import { Server } from "socket.io";
import { ChatEventTypes } from "../constants/types";
import dbConnect from "../db/DbConnect";
import { Message } from "../models/message";

export const chatHandler = (io: Server) => {

    return io.of(/^\/channel-\d+$/)
        .on("connection", (socket) => {
            const newNamespace = socket.nsp;
            console.log(newNamespace.name);
            // broadcast to all clients in the given sub-namespace
            newNamespace.emit(ChatEventTypes.CHAT_MESSAGE, `channel enter : ${newNamespace.name}`);
            // 연결한 소켓에게 데이터 전달
            socket.emit(ChatEventTypes.CHAT_MESSAGE, "admin 채널로 진입했습니다.");


            socket.on(ChatEventTypes.CHAT_MESSAGE, (message: any) => {

                dbConnect.getCollection("message").insertOne({
                    userId : "TEST001",
                    content : message
                });
                socket.emit(ChatEventTypes.CHAT_MESSAGE, message);
            });

            socket.on(ChatEventTypes.ROOM_CHAT_MESSAGE, (room: any, data) => {
                console.log(room, data);
                // 서버에 ROOM 정보를 UUID 생성 해서 Client에서 다시 주는 방식 으로
                newNamespace.in(room).emit(ChatEventTypes.ROOM_CHAT_MESSAGE, data);
            });

            socket.on(ChatEventTypes.ENTER_ROOM, (roomId) => {
                // TODO: 채널, ROOM 정보 저장
                console.log("socket.nsp.name: ", socket.nsp.name, roomId);
                // 모든 방에서 나가고 다시 한 방에만 입장 한다.
                socket.leaveAll();
                socket.join(roomId);
            });
            socket.on(ChatEventTypes.LEAVE_ROOM, (roomId) => {
                socket.leave(roomId);
            });
            socket.on(ChatEventTypes.LEAVE_ALL_ROOM, () => {
                socket.leaveAll();
            });



        });


};