import { configure } from '@storybook/angular';
import { setConsoleOptions } from '@storybook/addon-console';

setConsoleOptions({
  panelExclude: [],
});



function loadStories() {
  require('../src/stories/index.ts');
}

configure(loadStories, module);