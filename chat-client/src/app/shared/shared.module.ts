import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


const COMMON_MODULE = [
    CommonModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule,
];


@NgModule({
    // declarations: [],
    imports: COMMON_MODULE,
    exports: COMMON_MODULE,
    // providers: [],
})
export class SharedModule { }
