import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'chat-message',
  templateUrl: './chat-message.component.html',
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class ChatMessageComponent {
  constructor() { }

}
