import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { faComment, faMicrophone, faImage, faSmile, faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import { SocketService, ChatEventTypes } from 'src/app/socket.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'chat-aside-right',
  templateUrl: './chat-aside-right.component.html',
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class ChatAsideRightComponent implements OnInit {
  faEllipsisV = faEllipsisV;
  faSmile = faSmile;
  faImage = faImage;
  faMicrophone = faMicrophone;
  faComment = faComment;
  constructor(private socketService: SocketService) { }
  socket;
  ngOnInit() {
    this.socket = this.socketService.connect();
    this.socketService
      .getChatMessage()
      .subscribe((res) => {
        // console.log(Zone.current);
        console.log('서버 응답임 :', res);
      });
    console.log(this.socket);

    this.socket.emit(ChatEventTypes.CHAT_MESSAGE, 'TEST0001 ㄹㄹㅇ니라인;ㅏ링ㄴ라;ㅣ');
    this.socket.emit(ChatEventTypes.CHAT_MESSAGE, 'TEST0002 ㄹㄹㅇ니라인;ㅏ링ㄴ라;ㅣ');
    this.socket.emit(ChatEventTypes.CHAT_MESSAGE, 'TEST0004 ㄹㄹㅇ니라인;ㅏ링ㄴ라;ㅣ');
    this.socket.emit(ChatEventTypes.ENTER_ROOM, '001');
  }
  onTest($event) {
    console.log($event.target.value);
    this.socket.emit(ChatEventTypes.CHAT_MESSAGE, $event.target.value);
  }

}
