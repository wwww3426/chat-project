import { Component, ChangeDetectionStrategy } from '@angular/core';
import { faEllipsisV, faImage, faMicrophone, faSmile } from '@fortawesome/free-solid-svg-icons';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'chat-message-header',
  templateUrl: './chat-message-header.component.html',
  changeDetection  : ChangeDetectionStrategy.OnPush
})
export class ChatMessageHeaderComponent {
  faEllipsisV = faEllipsisV;
  faSmile = faSmile;
  faImage = faImage;
  faMicrophone = faMicrophone;
  constructor() { }

}
