import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { faImage, faMicrophone, faSmile } from '@fortawesome/free-solid-svg-icons';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'chat-message-sender',
  templateUrl: './chat-message-sender.component.html',
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class ChatMessageSenderComponent {

  @Output()
  sendMessage  = new EventEmitter();
  private faSmile = faSmile;
  private faImage = faImage;
  private faMicrophone = faMicrophone;

}
