import { Component, OnInit } from '@angular/core';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'chat-user',
  templateUrl: './chat-user.component.html'
})
export class ChatUserComponent implements OnInit {
  faEllipsisV = faEllipsisV;

  constructor() { }

  ngOnInit() {
  }
 
}
