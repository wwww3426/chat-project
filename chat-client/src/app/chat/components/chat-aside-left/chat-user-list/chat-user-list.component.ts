import { Component, OnInit } from '@angular/core';
import { faCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'chat-user-list',
  templateUrl: './chat-user-list.component.html'
})
export class ChatUserListComponent implements OnInit {
  faCircle = faCircle;
  constructor() { }

  ngOnInit() {}

}
