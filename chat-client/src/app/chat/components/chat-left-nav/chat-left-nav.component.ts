import { Component, OnInit } from '@angular/core';
import { faPlus, faSquare } from '@fortawesome/free-solid-svg-icons';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'left-nav',
  templateUrl: './chat-left-nav.component.html'
})
export class ChatLeftNavComponent implements OnInit {
  faPlus = faPlus;
  faSquare = faSquare;


  constructor() { }

  ngOnInit() { }

  addChannel() {
    console.log('TEST');
  }

}
