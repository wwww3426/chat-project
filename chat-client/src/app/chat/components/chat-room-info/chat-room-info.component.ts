import { Component, OnInit } from '@angular/core';
import { faTimes, faUsers, faUserPlus, faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'chat-room-info',
  templateUrl: './chat-room-info.component.html'
})
export class ChatRoomInfoComponent implements OnInit {
  faTimes = faTimes;
  faUsers = faUsers;
  faUserPlus = faUserPlus;
  faSearch = faSearch;

  constructor() { }

  ngOnInit() { }

}
