import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatRoutingModule } from '@app/chat/chat-routing.module';
import { ChatContainerComponent } from '@app/chat/container/chat-container.component';
import { ChatAsideLeftComponent } from '@app/chat/components/chat-aside-left/chat-aside-left.component';
import { ChatAsideRightComponent } from '@app/chat/components/chat-aside-right/chat-aside-right.component';
import { ChatRoomInfoComponent } from '@app/chat/components/chat-room-info/chat-room-info.component';
import { ChatUserComponent } from '@app/chat/components/chat-aside-left/chat-user/chat-user.component';
import { ChatUserSearchComponent } from '@app/chat/components/chat-aside-left/chat-user-search/chat-user-search.component';
import { ChatUserListComponent } from '@app/chat/components/chat-aside-left/chat-user-list/chat-user-list.component';
import { ChatMessageHeaderComponent } from '@app/chat/components/chat-aside-right/chat-message-header/chat-message-header.component';
import { ChatMessageComponent } from '@app/chat/components/chat-aside-right/chat-message/chat-message.component';
import { ChatMessageSenderComponent } from '@app/chat/components/chat-aside-right/chat-message-sender/chat-message-sender.component';
import { SharedModule } from '@app/shared/shared.module';
import { ChatLeftNavComponent } from './components/chat-left-nav/chat-left-nav.component';


const COMPONENTS = [
  ChatLeftNavComponent,
  ChatAsideLeftComponent,
  ChatAsideRightComponent,
  ChatRoomInfoComponent,
  ChatUserComponent,
  ChatUserSearchComponent,
  ChatUserListComponent,
  ChatMessageHeaderComponent,
  ChatMessageComponent,
  ChatMessageSenderComponent,
  ChatContainerComponent,
];


@NgModule({
  declarations: COMPONENTS,
  imports: [
    SharedModule,
    ChatRoutingModule
  ],
  exports : [
    ...COMPONENTS
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChatModule { }
