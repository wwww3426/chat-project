import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'chat-container',
  template: `
    <left-nav></left-nav>
    <chat-aside-left></chat-aside-left>
    <chat-aside-right></chat-aside-right>
    <chat-room-info></chat-room-info>
  `,
  styles: []
})
export class ChatContainerComponent implements OnInit {

  constructor(private store: Store<any>) { }

  ngOnInit() {
  }

}
