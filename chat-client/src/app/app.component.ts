import { Component, OnInit } from '@angular/core';
import { SocketService, ChatEventTypes } from './socket.service';
import { faEllipsisV, faCircle, faSmile , faImage, faMicrophone, faComment, faPlus, faSquare } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  faEllipsisV = faEllipsisV;
  faCircle = faCircle;
  faSmile = faSmile;
  faImage = faImage;
  faMicrophone = faMicrophone;
  faComment = faComment;

  faPlus = faPlus;
  faSquare  = faSquare;

  constructor(private socketService: SocketService) {

  }
  socket;
  title = 'chat-client';
  ngOnInit(): void {

  }

  onSend($event) {
    this.socket.emit(ChatEventTypes.ROOM_CHAT_MESSAGE, '001', $event.target.value);
  }

}
