import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs';
import { connect } from 'socket.io-client';


export enum ChatEventTypes {
  ENTER_ROOM = '[CHAT] ENTER ROOM',
  LEAVE_ROOM = '[CHAT] LEAVE ROOM',
  LEAVE_ALL_ROOM = '[CHAT] LEAVE ALL ROOM',
  CHAT_MESSAGE = '[CHAT] CHAT_MESSAGE',
  ROOM_CHAT_MESSAGE = '[CHAT] ROOM_CHAT_MESSAGE'
}

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private WS_URL = 'ws://localhost:3000/channel-002';
  socketMap: { [key: string]: Subject<any> };
  socket: SocketIOClient.Socket;
  constructor(private ngZone: NgZone) {
    this.socketMap = window['socketMap'] = {};
  }

  getChatMessage() {
    return this.socketMap[ChatEventTypes.CHAT_MESSAGE];
  }
  connect(url?: string) {
    this.ngZone.runOutsideAngular(() => {
      this.socket = connect(url || this.WS_URL, {
        transports: ['websocket']
      });
      this.socketMap[ChatEventTypes.CHAT_MESSAGE] = new Subject();
      this.socketMap[ChatEventTypes.ROOM_CHAT_MESSAGE] = new Subject();
    });

    this.socket.on(ChatEventTypes.CHAT_MESSAGE, (res) => {
      this.socketMap[ChatEventTypes.CHAT_MESSAGE].next(res);
    });
    this.socket.on(ChatEventTypes.ROOM_CHAT_MESSAGE, (res) => {
      this.socketMap[ChatEventTypes.ROOM_CHAT_MESSAGE].next(res);
    });
    return this.socket;
  }


}
