import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { exhaustMap, map, tap } from 'rxjs/operators';
import { Login, LoginActionsUnion, LoginActionTypes, LoginFail, LoginSuccess } from '../actions/login.actions';
import { Credentials } from '../models/user';
import { LogInService } from '../service/login.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {

  @Effect()
  login$ = this.actions$.pipe(
    ofType(LoginActionTypes.Login),
    map((action: Login) => action.payload.credentials),
    exhaustMap((credentials: Credentials) => this.logInService.login(credentials)),
    map((isLogin) => isLogin ?
      new LoginSuccess({ user: { name: 'TEST' } })
      : new LoginFail({ error: new Error('로그인 실패') }))
  );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType(LoginActionTypes.LoginSuccess),
    tap(() => this.router.navigate(['/register']))
  );

  @Effect({ dispatch: false })
  loginFail$ = this.actions$.pipe(
    ofType(LoginActionTypes.LoginFail),
    tap(() => this.router.navigate(['/']))
  );


  constructor(private actions$: Actions<LoginActionsUnion>
    , private logInService: LogInService
    , private router: Router) { }

}
