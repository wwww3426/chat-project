import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Credentials } from '../models/user';
import { interval, of } from 'rxjs';
import { take, first, map, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LogInService {

    private URL = '';

    constructor(private http: HttpClient) { }
    /**
     *
     * @param credentials
     */
    public login(credentials: Credentials) {
        return interval(1000).pipe(
            take(1),
            map(_ => true),
            tap(_ => console.log('TEST', _ )),
        );
        // return this.http.post<Credentials>(this.URL, credentials);
    }


}

