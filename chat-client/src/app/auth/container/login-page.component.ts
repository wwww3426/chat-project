import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Credentials } from '../models/user';
import { Login } from '../actions/login.actions';

@Component({
  selector: 'app-login-page',
  template: `
    <app-login (submitted)="onSubmit($event)" ></app-login>
  `,
})
export class LoginPageComponent implements OnInit {

  constructor(private store: Store<any>) { }

  ngOnInit() { }

  onSubmit(credentials: Credentials) {
    this.store.dispatch(new Login({ credentials }));
  }

}
