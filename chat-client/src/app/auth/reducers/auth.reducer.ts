import { LoginActionsUnion, LoginActionTypes } from '../actions/login.actions';
import { User } from '../models/user';


export interface State {
  user: User | null;
}

export const initialState: State = {
  user: null,
};

export function reducer(state = initialState, action: LoginActionsUnion): State {
  switch (action.type) {
    case LoginActionTypes.LoginSuccess: {
      return {
        ...state,
        user: action.payload.user,
      };
    }
    default:
      return state;
  }
}
