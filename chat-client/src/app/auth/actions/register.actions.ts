import { Action } from '@ngrx/store';

export enum RegisterActionTypes {
  UserRegister = '[Register] User Registers'
}

export class UserRegister implements Action {
  readonly type = RegisterActionTypes.UserRegister;
}

export type RegisterActions = UserRegister;
