import { Action } from '@ngrx/store';

export enum UserActionTypes {
  UserUpdate = '[User] User Update'
}

export class UserUpdate implements Action {
  readonly type = UserActionTypes.UserUpdate;
}

export type UserActions = UserUpdate;
