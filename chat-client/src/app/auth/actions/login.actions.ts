import { Action } from '@ngrx/store';
import { Credentials, User } from '@app/auth/models/user';

export enum LoginActionTypes {
  Login = '[Login] Login',
  LoginSuccess = '[Login] Login Success',
  LoginFail = '[Login] Login Fail'
}

export class Login implements Action {
  readonly type = LoginActionTypes.Login;
  constructor(public payload: { credentials: Credentials }) {}
}


export class LoginSuccess implements Action {
  readonly type = LoginActionTypes.LoginSuccess;
  constructor(public payload: { user: User }) {}
}

export class LoginFail implements Action {
  readonly type = LoginActionTypes.LoginFail;
  constructor(public payload: { error: any }) {}
}
export type LoginActionsUnion = Login | LoginSuccess | LoginFail;
