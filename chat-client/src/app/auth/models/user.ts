export interface Credentials {
    id: string;
    password: string;
}

export interface User {
    name: string;
}
