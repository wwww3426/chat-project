import { CommonModule } from '@angular/common';
import { storiesOf } from '@storybook/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';


storiesOf('Login', module)
  .add('LoginComponent', () => ({
    component: LoginComponent,
    moduleMetadata: {
      imports: [CommonModule, ReactiveFormsModule , StoreModule.forRoot({}), StoreDevtoolsModule.instrument()],
      declarations: [LoginComponent],
    }
  }));
