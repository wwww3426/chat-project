import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      id : [''],
      password : [''],
      email : [''],
      emailConfirm : [''],
    });
  }

  register() {
    const  { id, password , email , emailConfirm} = this.registerForm.controls;
    // this.store.dispatch(new RegisterAction());
  }

}
