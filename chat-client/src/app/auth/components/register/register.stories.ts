import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { storiesOf, addDecorator } from '@storybook/angular';
import { RegisterComponent } from './register.component';



storiesOf('register', module)
  .add('RegisterComponent', () => ({
    component: RegisterComponent,
    moduleMetadata: {
      imports: [CommonModule, ReactiveFormsModule],
      declarations: [RegisterComponent],
    }
  }));
