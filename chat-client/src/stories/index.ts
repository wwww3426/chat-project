import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { storiesOf, addDecorator } from '@storybook/angular';
import { withBackgrounds } from '@storybook/addon-backgrounds';
import { withNotes } from '@storybook/addon-notes';
// import { withConsole , setConsoleOptions } from '@storybook/addon-console';
import { LeftNavComponent } from 'src/app/left-nav/left-nav.component';
import { ChatContainerComponent } from 'src/app/chat-container/chat-container.component';
import { ChatAsideLeftComponent } from 'src/app/chat-container/chat-aside-left/chat-aside-left.component';
import { ChatAsideRightComponent } from 'src/app/chat-container/chat-aside-right/chat-aside-right.component';
import { ChatRoomInfoComponent } from 'src/app/chat-container/chat-room-info/chat-room-info.component';
import { ChatUserComponent } from 'src/app/chat-container/chat-aside-left/chat-user/chat-user.component';
import { ChatUserSearchComponent } from 'src/app/chat-container/chat-aside-left/chat-user-search/chat-user-search.component';
import { ChatUserListComponent } from 'src/app/chat-container/chat-aside-left/chat-user-list/chat-user-list.component';
import { ChatMessageHeaderComponent } from 'src/app/chat-container/chat-aside-right/chat-message-header/chat-message-header.component';
import { ChatMessageComponent } from 'src/app/chat-container/chat-aside-right/chat-message/chat-message.component';
import { ChatMessageSenderComponent } from 'src/app/chat-container/chat-aside-right/chat-message-sender/chat-message-sender.component';

const components = [
  ChatContainerComponent,
  ChatAsideLeftComponent,
  ChatAsideRightComponent,
  ChatRoomInfoComponent,
  ChatUserComponent,
  ChatUserSearchComponent,
  ChatUserListComponent,
  ChatMessageHeaderComponent,
  ChatMessageComponent,
  ChatMessageSenderComponent
];


addDecorator(withNotes);
addDecorator(withBackgrounds([
  { name: 'left-nav', value: '#37375D' }
]));

storiesOf('chennel', module)
  .add('AppComponent', () => ({
    component: LeftNavComponent,
    moduleMetadata: {
      imports: [FontAwesomeModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [...components],
    }
  }), {
      notes: '왼쪽 메뉴 - 채널을 추가하고 \n 채널 목록 아이콘 리스트를 출력한다.'
    })
  .add('ChatAsideLeftComponent', () => ({
    component: ChatAsideLeftComponent,
    moduleMetadata: {
      imports: [FontAwesomeModule],
      declarations: [...components],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }
  }));


